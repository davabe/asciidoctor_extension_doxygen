require 'asciidoctor'
require 'asciidoctor/extensions'
require 'nokogiri'

Asciidoctor::Extensions.register do
  inline_macro DoxygenInlineMacro
end

class DoxygenInlineMacro < Asciidoctor::Extensions::InlineMacroProcessor
  use_dsl

  named :doxygen
  parse_content_as :text

  @@doc = Hash.new

  def process parent, target, attrs
    doxygen_tag_files = parent.document.attributes.select { |key, value|
      if key.to_s.match(/^doxygen_tag_file_.+/)
        if File.file?(value)
          next(true)
        else
          puts "DoxygenInlineMacro: WARNING: Tag file #{value} not found"
        end
      end
      next(false)
    }
    doxygen_pdf_file = parent.document.attributes.fetch('doxygen_pdf_file', false)

    if (text = attrs['text']).empty?
      text = target
    end

    if doxygen_pdf_file
      (create_anchor parent, text, type: :link, target: doxygen_pdf_file).render
    else
      doxygen_html parent, target, attrs, text, doxygen_tag_files
    end
  end

  def doxygen_html parent, target, attrs, text, doxygen_tag_files
    link = nil
    doxygen_tag_files.each{ |key, doxygen_tag_file|
      doxygen_key = key.match(/^doxygen_tag_file_(.+)/).captures.first
      doxygen_path = parent.document.attributes.fetch("doxygen_path_#{doxygen_key}")
      @@doc[doxygen_key] ||= DoxygenDoc.new(doxygen_tag_file, doxygen_path)
      link ||= @@doc[doxygen_key].find_link(target)
    }

    if link.nil?
      puts "DoxygenInlineMacro: WARNING: #{target}[#{text}] doxygen link not found"
      return create_anchor parent, "["+text+"]", type: :link, target: "#"
    end

    create_anchor parent, text, type: :link, target: link
  end
end

class DoxygenDoc
  def initialize( doxygen_tag_file, doxygen_path )
    @doc = File.open(doxygen_tag_file) { |f| Nokogiri::XML(f) }
    @doxygen_path = doxygen_path
  end

  def find_link(target)
    node = @doc.at_xpath("//name[text()='#{target}']/..")
    if node.nil?
      return nil
    end

    f = node.xpath("filename/text()").first

    if f
      fixed_target = f.to_s
      fixed_target = f.to_s + ".html" if f.to_s[".html"].nil?
    else
      af = node.xpath("anchorfile/text()").first
      a = node.xpath("anchor/text()").first
      fixed_target = af.to_s + "#" + a.to_s if af
    end

    return @doxygen_path + fixed_target
  end
end
